In this repository you can find the needed tools, to recreate the classification of different organic substances with the aid of a VOC sensor.
You can find further information in the paper 'XYZ'

There are three folders:

- datasets: contains the ready-to-be-used dataset for classification purposes

- Taking_measurements: here you will find everything you need, to perform your own measurements. i.e. The driver for the microcontroller ('IoTOctopus'), the python script to run the measurements and a python script to create your own dataset from your measurements

- Evaluating_measurements: here you find a Jupyter notebook for a brief classification performance analysis. In future there will be two additional notenooks. A Hyperparameter Tuning notebook and a feature reduction notebook.


Feel free to adapt the code to your own needs.
It would be great to share your datasets with the rest of the community, in order to create a larger database for further research.