import serial 
import serial.tools.list_ports
import time
import os.path
import pandas as pd 
import numpy as np
import math
from datetime import datetime
import warnings

while 0:
    arduino_ports = [
        p.device
        for p in serial.tools.list_ports.comports()
        if 'Arduino' in p.description  # may need tweaking to match new arduinos
    ]
    if not arduino_ports:
        raise IOError("No Arduino found")
    if len(arduino_ports) > 1:
        warnings.warn('Multiple Arduinos found - using the first')

    ser = serial.Serial(arduino_ports[0])

# Select the right USB port, the desired sensor name (for saving purposes)
port = '/dev/cu.usbserial-010E5539'
sensor_number = '3'

### Change the sensor parameters here ###
configuration = {
    'Start_Temp': 200,                          #between 0 and 400
    'Intervall': 5,                             #between 0 and 100
    'End_Temp': 400,                            #between Start_Temp and 400
    'HTDR': 500, #gas_warm                      #between 50 and 600
    'CTDR': 50, #gas_cold                       #between 0 and 600
    'TRSH': 0.002,                              #between 0 and 1
    'NEUTR': True,                              # True or False
    'SAT': True,                                # True or False
    'ENV': True                                 # True or False
}
preheating = 300 #in seconds
#--------------------------------------------------

print(configuration)
print('Preheating in sec = ' + str(preheating))
print('Number of Measurements = ' +str(number_measurements))


number_temperatures = math.trunc((configuration['End_Temp'] - configuration['Start_Temp']) / configuration['Intervall'] )+ 1

substance = input('Enter the name of the substance you want to measure: ')
number_measurements = input('Enter the desired number of measurement series')


while not os.path.exists(port):
    time.sleep(0.1)

ser = serial.Serial (port = port, 
                    baudrate = 115200, 
                    parity=serial.PARITY_EVEN, 
                    stopbits=serial.STOPBITS_ONE, 
                    bytesize=serial.EIGHTBITS, 
                    timeout = 10,
                    dsrdtr = False)

s = 0
global_list = []
value_list = []
env_list = []
proc_list = []
raw_list = []
number_list = []
series = 0
temperature = configuration["Start_Temp"]
counter = 0
save_counter = 0

now = datetime.now()
time_name = now.strftime("%Y%m%d%H%M")

if configuration["NEUTR"]:
    name_neutr = '_NEUTR_'
else:
    name_neutr = '_noNEUTR_'
save_name = 'sensor_'+sensor_number+'_'+ substance + name_neutr + time_name + '.csv'
save_name_sub = 'subset_' + save_name
save_name_proc = 'processed_' + save_name

#Define the configuration string, which is sent to the sensor
temp_list = []
configuration_string = 'TEMP:'
while temperature <= configuration["End_Temp"]:
    configuration_string = configuration_string + (str(temperature)+",")
    temp_list.append(temperature)
    temperature = temperature + configuration["Intervall"]
configuration_string = configuration_string[:-1]
configuration_string = configuration_string + (';HTDR:' + str(configuration["HTDR"]))
configuration_string = configuration_string + (';CTDR:' + str(configuration["CTDR"]))
configuration_string = configuration_string + (';TRSH:' + str(configuration["TRSH"]))
if configuration['NEUTR']:
    configuration_string = configuration_string + (';NEUTR')
if configuration['SAT']:
    configuration_string = configuration_string + (';SATUR')
if configuration['ENV']:
    configuration_string = configuration_string + (';ENV')
configuration_string = configuration_string + (';;')

print(configuration_string)


#"Handshake" between PC and sensor, establish Connection
while (s != 'Nose!\r\n'):
    s = ser.readline().decode('utf-8')
    ser.write('Yes, who is this?'.encode('utf-8'))

#Send Configuration to the sensor and preheat the sensor
output_buffer_size = 120
while(s != 'STATE:PREP\r\n'):
    configuration_string_chunk = configuration_string
    while len(configuration_string_chunk) > 1 :
        until = min(output_buffer_size, len(configuration_string_chunk))
        ser.write(configuration_string_chunk[0:until].encode('utf-8'))
        time.sleep(1)
        configuration_string_chunk = configuration_string_chunk[until:]
    s = ser.readline().decode('utf-8')


time.sleep(preheating)

#Sensor is put in the READY State and measurements can be performed
while(s != 'STATE:READY\r\n'):
    ser.write('GETREADY'.encode('utf-8'))
    s = ser.readline().decode('utf-8')

#Measurements if the environment is put into consideration
if configuration["ENV"]:
    input('Press Enter to start measurement of environment')
    #Measurement with neutralization phase
    raw_sub_list = []
    if configuration["NEUTR"]:
        while(s != 'STATE:NEUTR_ENV\r\n'):
            ser.write('START'.encode('utf-8'))
            s = ser.readline().decode('utf-8')
      
    
    #Measurement without neutralization phase
    else:
        while(s != 'STATE:MEAS_ENV\r\n'):
            ser.write('START'.encode('utf-8'))
            s = ser.readline().decode('utf-8')
    
    while(True):
            s = ser.readline().decode('utf-8')
            print(s)
            if s.startswith('TEMP'):
                value = float(s.replace('TEMP:', ''))
                raw_sub_list.append(value)
            if s.startswith('HUMI'):
                value = float(s.replace('HUMI:', ''))
                raw_sub_list.append(value)
            if s.startswith('PRES'):
                value = float(s.replace('PRES:', ''))
                raw_sub_list.append(value)
            if s.startswith('VAL'):
                value = float(s.replace('VAL:', ''))
                env_list.append(value)
            if s == 'STATE:PAUSE\r\n':
                raw_sub_list = raw_sub_list + env_list + [configuration_string + 'Preheating:' + str(preheating)]
                break 

raw_list.append(raw_sub_list)
                    
input('Place the sensor above substance and press Enter')

for i in range(number_measurements):
    print(i)
    time.sleep(5)
    raw_sub_list = []
    #Only the substance is measured
    #Measurement with neutralization phase
    if configuration["NEUTR"]:
        while(s != 'STATE:NEUTR_SUB\r\n'):
            ser.write('CONT'.encode('utf-8'))
            s = ser.readline().decode('utf-8')

    #Measurement without neutralization phase
    else:
        while(s != 'STATE:MEAS_SUB\r\n'):
            ser.write('CONT'.encode('utf-8'))
            s = ser.readline().decode('utf-8')
        
    while(True):
        s = ser.readline().decode('utf-8')
        print(s)
        if s.startswith('TEMP'):
            value = float(s.replace('TEMP:', ''))
            raw_sub_list.append(value)
        if s.startswith('HUMI'):
            value = float(s.replace('HUMI:', ''))
            raw_sub_list.append(value)
        if s.startswith('PRES'):
            value = float(s.replace('PRES:', ''))
            raw_sub_list.append(value)
        if s.startswith('VAL'):
            value = float(s.replace('VAL:', ''))
            value_list.append(value)
        if s == 'STATE:PAUSE\r\n':
            raw_sub_list = raw_sub_list + value_list + [configuration_string + 'Preheating:' + str(preheating)]
            break 


    processed_list = list(( np.array(value_list) - np.array(env_list) ) / np.array(env_list))
    global_list.append(value_list)
    raw_list.append(raw_sub_list)
    proc_list.append(processed_list)
    number_list.append(i+1)
    save_counter += 1
    if save_counter > 0:
        df_sub = pd.DataFrame(global_list, index = number_list, columns = temp_list)
        df_sub.index.name = 'series'
        df_sub.to_csv('./subset/'+save_name_sub)
        df_proc = pd.DataFrame(proc_list, index = number_list, columns = temp_list)
        df_proc.index.name = 'series'
        df_proc.to_csv('./processed_data/'+save_name_proc)
        save_counter = 0

    value_list = []
    counter = 0

complete_list = ['Temp', 'Humi', 'Pres'] + temp_list + ['configuration']
number_list = [0] + number_list
df = pd.DataFrame(raw_list, index = number_list, columns = complete_list)
df.index.name = 'series'
print(df)
df.to_csv('./raw_data/'+save_name)

#close the Serial Connection and Reset the Board
while(ser.isOpen()):
    ser.close()
ser.dsrdtr = True
ser.open()
ser.close()