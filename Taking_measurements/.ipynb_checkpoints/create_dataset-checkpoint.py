import numpy as np
import pandas as pd
import os
import joblib

csv_data_path = './working_data/'

list_csv_files = os.listdir(csv_data_path)
if '.DS_Store' in list_csv_files:
    list_csv_files.remove('.DS_Store')

df_names=[]
for l in list_csv_files:
    df_names.append(l[17:-4])

list_of_dataframes = []
df = pd.DataFrame()
sensor = 0
for filename in list_csv_files:
    list_of_dataframes.append(pd.read_csv(csv_data_path + filename).set_index('series'))
    frame = pd.read_csv(csv_data_path + filename).set_index('series')
    if 'wine' in filename:
        frame['Class'] = 1
    elif 'garlic' in filename:
        frame['Class'] = 2
    elif 'vinegar' in filename:
        frame['Class'] = 3
    if 'sensor_2' in  filename:
        sensor = 2
    else:
        sensor = 3
    frame['Sensor'] = sensor
    df = df.append(frame)

print(df)

#df.to_csv('./working_set/raw_sensor3_complete.csv')
