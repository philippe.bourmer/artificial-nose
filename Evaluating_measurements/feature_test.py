import numpy as np
import pandas as pd
import os
import joblib
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_regression
from sklearn import ensemble
from sklearn.linear_model import LogisticRegression


pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 5)

df_train = pd.read_csv('./working_set/proc_sensor3_complete.csv').set_index('series').drop(['Sensor'], axis =1)
df_test = pd.read_csv('./working_set/proc_sensor2_complete.csv').set_index('series').drop(['Sensor'], axis =1)

model = RandomForestClassifier()

X_train = df_train.drop(['Class'], axis = 1)
y_train = df_train['Class']
X_test = df_test.drop(['Class'], axis = 1)
y_test = df_test['Class']

#Train the scaler, PCA and SVC_Classifier
scaler =  MinMaxScaler()
scaler.fit(X_train)
train = scaler.transform(X_train)
X_train = pd.DataFrame(train, columns = X_train.columns)

test = scaler.transform(X_test)
X_test = pd.DataFrame(test, index = X_test.index, columns = X_test.columns)

accuracy_list = []
for i in list(X_train.columns):
    for j in list(X_train.columns):
        if i != j:
            X_train_s = df_train[[i, j]]
            X_test_s = df_test[[i, j]] 

            model.fit(X_train_s, y_train)
            accuracy_list.append([i, j, model.score(X_test_s, y_test)])
            #print('Accuracy score with features ', i, ', ', j, ': ', model.score(X_test_s, y_test))

df_accuracy = pd.DataFrame(accuracy_list, columns = ['Feature 1', 'Feature 2', 'Accuracy'])
print(df_accuracy.sort_values(by = ['Accuracy']))
df_accuracy.to_csv('sensor_3_rf.csv')