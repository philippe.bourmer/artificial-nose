import numpy as np
import pandas as pd
import os
import joblib
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from sklearn import ensemble


plot = 0

used_set = {'Method': 'raw', #proc or raw
            'Sensor': 'sensor2_3', #allSensors, sensor2 or sensor3
            'Data': 'complete'} #old or complete


pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 5)
csv_data_path = './working_set/'

df_train = pd.read_csv('./working_set/proc_sensor3_complete.csv').set_index('series').drop(['Sensor'], axis =1)
df_test = pd.read_csv('./working_set/proc_sensor2_complete.csv').set_index('series').drop(['Sensor'], axis =1)

X_train = df_train[['210','390']]
y_train = df_train['Class']
X_test = df_test[['210','390']]
y_test = df_test['Class']

#X_train = df_train.drop(['Class'], axis = 1)
#y_train = df_train['Class']
#X_test = df_test.drop(['Class'], axis = 1)
#y_test = df_test['Class']

scaler =  MinMaxScaler()
scaler.fit(X_train)
train = scaler.transform(X_train)
X_train = pd.DataFrame(train, columns = X_train.columns)

test = scaler.transform(X_test)
X_test = pd.DataFrame(test, columns = X_test.columns)

#X_train = df_train[['210','390']]
#X_test = df_test[['210','390']]

clf = ensemble.GradientBoostingClassifier(n_estimators=400, max_depth=5, min_samples_split=2, learning_rate=0.1)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(clf.score(X_test, y_test))

if plot:
    fig = plt.figure(figsize=(15,10.80))
    ax = fig.add_subplot(111) 
    ax.set_xlabel('Principal Feature 1', fontsize = 15)
    ax.set_ylabel('Principal Feature 2', fontsize = 15)
    ax.set_title('2 Features', fontsize = 20)
    legend_elements = [Line2D([0], [0], marker='o', c = 'red', label='wine', markersize=8),
                    Line2D([0], [0], marker='o', c = 'blue', label='garlic', markersize=8),
                        Line2D([0], [0], marker='o', c = 'green', label='vinnegar', markersize=8)
                ]
    ax.legend(handles=legend_elements,loc='upper center', bbox_to_anchor=(0.5, -0.05),
        fancybox=True, shadow=True, ncol=5)
    ax.grid()
    colors = ['red', 'blue', 'green', 'lightcoral', 'lightblue', 'lightgreen']
    ax.scatter(X_train['210'], X_train['390'], c = np.array(colors)[y_train-1], s = 35)
    ax.scatter(X_test['210'], X_test['390'], c = np.array(colors)[y_test+2] , s = 35)
    plt.show()

