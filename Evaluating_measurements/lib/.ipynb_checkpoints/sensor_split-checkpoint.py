import pandas as pd

def two_sensor_split(df, train_sensor, test_sensor):
    df_train = df.loc[df['Sensor'] == train_sensor]
    df_test = df.loc[df['Sensor'] == test_sensor]
    X_train = df_train.drop(['Class', 'Sensor'], axis = 1)
    y_train = df_train['Class']
    X_test = df_test.drop(['Class', 'Sensor'], axis = 1)
    y_test = df_test['Class']
    
    return(X_train, X_test, y_train, y_test)