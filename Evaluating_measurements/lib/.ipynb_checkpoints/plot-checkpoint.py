import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.lines import Line2D
import numpy as np

def decision_boundaries_2d(X, classifier, X_train, X_test, y_train, y_test, score):
    plt.rcParams['figure.figsize'] = [15, 10]
    h = .02  # step size in the mesh
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                        np.arange(y_min, y_max, h))
    figure = plt.figure()
    ax = plt.subplot()
    cm = ListedColormap(['#FF0000', '#C0BA35', '#387919'])
    cm_bright = ListedColormap(['#FF0000', '#C0BA35', '#387919'])

    if hasattr(classifier, "decision_function"):
        Z = classifier.decision_function(np.c_[xx.ravel(), yy.ravel()])
    else:
        Z = classifier.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]

    Z = classifier.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    ax.contourf(xx, yy, Z, cmap=cm, alpha=.3)

    # Plot the training points
    ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright,
                edgecolors='k', alpha = 0.6)
    # Plot the testing points
    ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright,
                edgecolors='k', alpha=1, marker = 'X')

    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xticks(())
    ax.set_yticks(())

    legend_elements = [ Line2D([0], [0], marker='o', c = 'w', label='Train Datapoints',
                          markerfacecolor='black', markersize=8, alpha = 0.6),
                       Line2D([0], [0], marker='X', c = 'w', label='Test Datapoints',
                          markerfacecolor='black', markersize=8)
                   ]
    ax.legend(handles=legend_elements,#loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=1)

    ax.text(xx.max() - .1, yy.min() + .1, ('$F_1=$%.2f' % score).lstrip('0'),
            size=15, horizontalalignment='right')