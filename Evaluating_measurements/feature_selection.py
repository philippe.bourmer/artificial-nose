import numpy as np
import pandas as pd
import os
import joblib
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_regression
from sklearn import ensemble
from sklearn.linear_model import LogisticRegression


pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 5)

df_train = pd.read_csv('./working_set/proc_sensor3_complete.csv').set_index('series').drop(['Sensor'], axis =1)
df_test = pd.read_csv('./working_set/proc_sensor2_complete.csv').set_index('series').drop(['Sensor'], axis =1)

model = LogisticRegression(**{'penalty': 'none', 'solver': 'newton-cg'})

X_train = df_train.drop(['Class'], axis = 1)
y_train = df_train['Class']
X_test = df_test.drop(['Class'], axis = 1)
y_test = df_test['Class']

#Train the scaler, PCA and SVC_Classifier
scaler =  MinMaxScaler()
scaler.fit(X_train)
train = scaler.transform(X_train)
X_train = pd.DataFrame(train, columns = X_train.columns)

test = scaler.transform(X_test)
X_test = pd.DataFrame(test, index = X_test.index, columns = X_test.columns)

#Evaluate on reduced features
for k in range(1,X_train.shape[1]):
    bestfeatures = SelectKBest(k=k, score_func=f_regression)
    X_train_skb = bestfeatures.fit_transform(X_train, y_train)
    X_test_skb = bestfeatures.transform(X_test)
    model.fit(X_train_skb, y_train)
    #print('Score at k =', k, ':', model.score(X_test_skb, y_test))

model.fit(X_train, y_train)
#print('Score at k = all :', model.score(X_test, y_test))

#apply SelectKBest class to sort features by importance
bestfeatures = SelectKBest(score_func=chi2 , k=2)
fit = bestfeatures.fit(X_train, y_train)
dfscores = pd.DataFrame(fit.scores_)
dfcolumns = pd.DataFrame(X_train.columns)
#concat two dataframes for better visualization 
featureScores = pd.concat([dfcolumns,dfscores],axis=1)
featureScores.columns = ['Specs','Score']  #naming the dataframe columns
print(featureScores.sort_values(by=['Score'], ascending = False))  #print 10 best features


df_train_v = pd.read_csv('./working_set/proc_sensor3_complete.csv').set_index('series').drop(['Sensor'], axis =1)
df_test_v = pd.read_csv('./working_set/proc_sensor2_complete.csv').set_index('series').drop(['Sensor'], axis =1)


#Validation of result
model_v = LogisticRegression(**{'penalty': 'none', 'solver': 'newton-cg'})

X_train_v = df_train[['390','395']]
y_train_v = df_train['Class']
X_test_v = df_test[['390','395']]
y_test_v = df_test['Class']

#Train the scaler, PCA and SVC_Classifier
scaler_v =  MinMaxScaler()
scaler_v.fit(X_train_v)
train_v = scaler_v.transform(X_train_v)
X_train_v = pd.DataFrame(train_v, columns = X_train_v.columns)

test_v = scaler_v.transform(X_test_v)
X_test_v = pd.DataFrame(test_v, columns = X_test_v.columns)

model_v.fit(X_train_v, y_train_v)
#print('Validation Score :', model_v.score(X_test_v, y_test_v))